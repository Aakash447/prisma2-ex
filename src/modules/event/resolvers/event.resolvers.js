const getEvents = require('./getEvents');
const createEvent = require('./createEvent');

const resolvers = {
  Query: {
    getEvents,
  },
  Mutation: {
    createEvent,
  },
};

module.exports = resolvers;
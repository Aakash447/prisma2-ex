const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const logger = require('../../../logger');

const getUser = async (_, args, ctx) => {
  try {
    const events = await prisma.event.findMany({
      include: {
        creator: true,
      }
    });
    return events;
  } catch (err) {
    logger.error('Error from getEvents ==> ', err);
    throw err;
  }
}

module.exports = getUser;
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const logger = require('../../../logger');

const createUser = async (_, args, ctx) => {
  try {
    const event = await prisma.event.create({
  data: {
    eventName: args.eventName,
    creatorId: args.creatorId,
  },
})
return event;
  } catch (err) {
    logger.error('Error from createEvent ==> ', err);
    throw err;
  }
}

module.exports = createUser;
const { join } = require('path');
const { fileLoader, mergeResolvers, mergeTypes } = require('merge-graphql-schemas');
const { gql } = require('apollo-server-express');

const globalTypeDefs = gql`
  type Query
  type Mutation
`;

const scalerTypeDefs = gql`
  scalar JSON
  scalar DATETIME
`;

const typesArray = [
  ...fileLoader(join(__dirname, './**/*.graphql')),
  globalTypeDefs,
  scalerTypeDefs,
];

const resolverArray = fileLoader(join(__dirname, './**/*.resolvers.*'));

const typeDefs = mergeTypes(typesArray);
const resolvers = mergeResolvers(resolverArray);

module.exports = { typeDefs, resolvers };
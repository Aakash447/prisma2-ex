const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const logger = require('../../../logger');

const createUser = async (_, args, ctx) => {
  try {
    const user = await prisma.user.create({
  data: {
    email: args.email,
    name: args.name,
  },
})
return user;
  } catch (err) {
    logger.error('Error from createUser ==> ', err);
    throw err;
  }
}

module.exports = createUser;
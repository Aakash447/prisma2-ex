const createUser = require('./createUser');
const getUser = require('./getUser');
const getUserById = require('./getUserById')

const resolvers = {
  Query: {
    getUser,
    getUserById,
  },
  Mutation: {
    createUser,
  },
};

module.exports = resolvers;
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const logger = require('../../../logger');

const getUser = async (_, args, ctx) => {
  try {
    const user = await prisma.user.findUnique({
      where: {
        id: args.id,
      },
      include: {
        event: true,
      },
    })
    return user;
  } catch (err) {
    logger.error('Error from getUserById ==> ', err);
    throw err;
  }
}

module.exports = getUser;
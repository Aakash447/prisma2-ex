const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const logger = require('../../../logger');

const getUser = async (_, args, ctx) => {
  try {
    const users = await prisma.user.findMany({
      include: {
        event: true,
      }
    });
    return users;
  } catch (err) {
    logger.error('Error from getUser ==> ',err);
    throw err;
  }
}

module.exports = getUser;
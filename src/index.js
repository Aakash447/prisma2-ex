require('dotenv').config();
const cors = require('cors');
const http = require('http');
const express = require('express');
const { ApolloServer, AuthenticationError } = require('apollo-server-express');
const { makeExecutableSchema } = require('graphql-tools');
const createGraphQLLogger = require('graphql-log');
const { resolvers, typeDefs } = require('./modules');
// const { directiveResolvers } = require('./directives/auth-directive');
const logger = require('./logger');
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
// const { prisma } = require('../generated/prisma-client')

const app = express();

app.use(cors());
app.use(express.json({ limit: '10mb' }));
app.use(express.urlencoded({ limit: '10mb' }));

const logExecutions = createGraphQLLogger({
  logger: logger.info,
});

logExecutions(resolvers);

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
  // directiveResolvers,
});

const server = new ApolloServer({
  schema,
  introspection: true,
  playground: true,
  formatError: error => {
    const message = error.message
      .replace('Validation error: ', '');

    return {
      ...error,
      message,
    };
  },
  context: async ctx => ({
    req: ctx.req,
    prisma,
  }),
});

server.applyMiddleware({ app, path: '/graphql' });

const httpServer = http.createServer(app);

httpServer.listen(process.env.PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`🚀 Server ready at http://localhost:${process.env.PORT}/graphql`);
});

module.exports = app;